/**
 * 
 */
package com.parish.app.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.parish.app.service.AdminService;

/**
 * @author heman
 *
 */
@RestController()
@RequestMapping(value = "admin")
public class AdminController {
	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "/addTeacher.do", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String addTeacher(@RequestBody Map<String, String> request) {
		return adminService.addChruch(request);
	}

}
