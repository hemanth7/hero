/**
 * 
 */
package com.parish.app.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author heman
 *
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {
	@Autowired
	private CommonService commonService;
	/* (non-Javadoc)
	 * @see com.school.app.service.AdminService#addTeacher(java.util.Map)
	 */
	@Override
	public String addChruch(Map<String, String> request) {
		return commonService.add(null,null);
	}

}
