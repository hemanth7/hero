/**
 * 
 */
package com.parish.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author heman
 *
 */
@Repository("commonDao")
public class CommonDaoimpl implements CommonDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public String save(Object Entity) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(Entity);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
		return null;
	}

}
